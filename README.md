miniWEB
=======

FOR 被TX抛弃的linuxer!!!

该应用主要用于解决快捷迅速访问web应用的问题(效果见下面的连接)

miniWeb其实是一个基于webkit开发的微型'浏览器',该应用能够满足你同时登陆多个微信和QQ的需求,你只需要按照下方安装方式安装,即可变理想为现实 -_-

#安装方法
## linux下安装方式
```
$git clone https://github.com/larryhu/miniWEB.git
$cd miniWEB
$./start.sh #默认启动微信, 微信: start.sh wechat QQ: start.sh qq

#依赖环境安装:

$sudo apt-get install python-notify python-webkit 
```

so easy!!!

# 使用效果
效果如下:

以微信为例


登陆页

![登陆页](http://d.pcs.baidu.com/thumbnail/4dab2ccfdadb9aa6b4900e6a6c218948?fid=1107316546-250528-1058581885095558&time=1402146000&sign=FDTAER-DCb740ccc5511e5e8fedcff06b081203-Nv2va8ZMRTo62srrCGviLH%2BlxAc%3D&rt=sh&expires=2h&r=262281928&sharesign=unknown&size=c10000_u10000&quality=100 "登陆页")  

主页面

![主页面](http://d.pcs.baidu.com/thumbnail/16eaf3f146804b76d75c8f6c74a232e8?fid=1107316546-250528-178352556188609&time=1402146000&sign=FDTAER-DCb740ccc5511e5e8fedcff06b081203-NgbczM2%2BmZs55Dodc8HrCaHBRHg%3D&rt=sh&expires=2h&r=329917665&sharesign=unknown&size=c10000_u10000&quality=100 "主页面")

消息提醒

![消息提醒](http://d.pcs.baidu.com/thumbnail/8f5fac9e3a02a39a9679dca33804c6a3?fid=1107316546-250528-563814001429809&time=1402146000&sign=FDTAER-DCb740ccc5511e5e8fedcff06b081203-hV0fHkbYoCxXZ6B0JlSmkoFxE2k%3D&rt=sh&expires=2h&r=333179939&sharesign=unknown&size=c10000_u10000&quality=100 "消息提醒")

状态栏消息闪烁提醒

![状态栏消息闪烁提醒](http://d.pcs.baidu.com/thumbnail/f25276de696bd66e47027dbca2a60da2?fid=1107316546-250528-492926152695506&time=1402146000&sign=FDTAER-DCb740ccc5511e5e8fedcff06b081203-UjSOLCRAjupKaZJ2vgkr7TOn77U%3D&rt=sh&expires=2h&r=491441702&sharesign=unknown&size=c10000_u10000&quality=100 "状态栏消息闪烁提醒")


#问题反馈

可以在本该项目下创建 [issues](https://github.com/larryhu/miniWEB/issues)
也可以新浪微博[@___larry___](http://weibo.com/u/2250775857) 


# 玩的开心 ☻ 
